import React from 'react';
import axios from 'axios';
import { BootstrapTable, TableHeaderColumn, ClearSearchButton } from 'react-bootstrap-table'

class Supplier extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            sortName: '',
            sortOrder: ''
        }
    }

    componentDidMount = () => {
        axios.get('http://like.aalavai.com/api/apiindex')
            .then(response => {
                this.setState({
                    data: response.data.supplier
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    createCustomClearButton = () => {
        return (
            <ClearSearchButton
                btnText='Clear'
                btnContextual='btn-primary' />
        );
    }

    onSortChange = (sortName, sortOrder) => {
        this.setState({
            sortName,
            sortOrder
        });
    }

    render() {
        const { data } = this.state;
        const options = {
            clearSearch: true,
            clearSearchBtn: this.createCustomClearButton,
            SortOrder: this.state.sortOrder,
            SortName: this.state.sortName,
            onSortChange: this.onSortChange,
        };
        return (
            <div className="container">
                <img src='./supplier.png' alt='logo' />
                <h1>Supplier Details</h1>
                <hr /><br />
                <BootstrapTable data={data} pagination hover search searchPlaceholder='Search Here...' options={options} exportCSV>
                    <TableHeaderColumn dataField='Id' isKey dataSort width='90px'> ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='BuyerName' dataSort width='290px'>Buyer's Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='ComapnyName' dataSort width='290px'>Comapany Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='Phone1' dataSort>Phone No</TableHeaderColumn>
                    <TableHeaderColumn dataField='designation' dataSort>designation</TableHeaderColumn>
                    <TableHeaderColumn dataField='ContactPerson' dataSort>ContactPerson</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Supplier;
