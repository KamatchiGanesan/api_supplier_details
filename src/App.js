import logo from './logo.svg';
import './App.css';
import Supplier from './Components/Supplier';

function App() {
  return (
    <div className="App">
      <Supplier />
    </div>
  );
}

export default App;
